pub fn input_generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for s in input.split("\n") {
        result.push(s.parse::<i32>().unwrap())
    }

    result
}

pub fn solve_part1(input: &Vec<i32>) -> i32 {
    let mut total = 0;

    for module_mass in input {
        total += (module_mass / 3) - 2
    }

    total
}

pub fn solve_part2(input: &Vec<i32>) -> i32 {
    let mut total = 0;

    for module_mass in input {
        total += get_fuel(module_mass)
    }

    total
}

fn get_fuel(mass: &i32) -> i32 {
    let mut total_fuel = 0;

    let mass_fuel = (mass / 3) - 2;

    if mass_fuel > 0 {
        total_fuel += mass_fuel;

        total_fuel += get_fuel(&mass_fuel);
    }

    total_fuel
}
