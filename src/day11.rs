use super::computer::intcode::day11::Computer;
use std::collections::HashMap;

pub fn generator(input: &str) -> Vec<isize> {
    let mut result: Vec<isize> = Vec::new();

    for s in input.split(",") {
        result.push(s.parse::<isize>().unwrap())
    }

    result
}

#[derive(Debug, Clone)]
pub struct Painted {
    x: i64,
    y: i64,
    painted: i64,
}
#[derive(Debug)]
pub struct Cell {
    colour: i64,
    painted: i64,
}
#[derive(Debug)]
pub struct Grid {
    grid: HashMap<i64, HashMap<i64, Cell>>,
}

impl Grid {
    fn new() -> Grid {
        Grid { grid: HashMap::new() }
    }

    fn get_colour(&self, x: &i64, y: &i64) -> i64 {
        match self.grid.get(x) {
            Some(inner) => {
                match inner.get(y) {
                    Some(cell) => cell.colour,
                    None => {
                        // default colour is black, 0
                        0
                    }
                }
            }
            None => {
                // default colour is black, 0
                0
            }
        }
    }

    fn set_colour(&mut self, x: i64, y: i64, colour: i64) {
        let inner = self.grid.entry(x).or_insert(HashMap::new());
        // all cells start off black
        let cell = inner.entry(y).or_insert(Cell { colour: 0, painted: 0 });
        cell.colour = colour;
        cell.painted += 1;
    }

    fn get_painted(&mut self) -> Vec<Painted> {
        let mut painted: Vec<Painted> = vec![];

        for (x, inner) in self.grid.iter() {
            for (y, cell) in inner.iter() {
                painted.push(Painted { x: *x, y: *y, painted: cell.painted });
            }
        }

        painted
    }
}
pub struct Position {
    x: i64,
    y: i64,
}

pub fn part1(program: &Vec<isize>) -> i64 {
    // for knowing where on teh grid I am
    let mut position = Position { x: 0, y: 0 };
    let mut direction = 1;

    let mut grid = Grid::new();

    // boot up teh computer
    let mut computer = Computer::new();
    computer.set_program(program);

    loop {
        // if it finishes teh program
        if computer.finished {
            break;
        }

        let colour = grid.get_colour(&position.x, &position.y) as isize;
        computer.set_input(colour).run();

        let output = &computer.output;

        grid.set_colour(position.x, position.y, output[0] as i64);

        // get new direction
        match output[1] {
            0 => {
                if direction == 1 {
                    direction = 4
                } else {
                    direction -= 1
                }
            }
            1 => {
                if direction == 4 {
                    direction = 1
                } else {
                    direction += 1
                }
            }
            // should not happen
            _ => {}
        }

        // get new position
        match direction {
            // north
            1 => position.y += 1,
            // east
            2 => position.x += 1,
            // south
            3 => position.y -= 1,
            // west
            4 => position.x -= 1,
            // should not happen
            _ => {}
        }

        computer.clear_output();
    }

    grid.get_painted().iter().filter(|x| x.painted > 0).cloned().collect::<Vec<Painted>>().len() as i64
}

#[derive(Debug)]
pub struct Position2 {
    x: i64,
    y: i64,

    x_min: i64,
    y_min: i64,

    x_max: i64,
    y_max: i64,
}
impl Position2 {
    fn min(&mut self) -> &mut Position2 {
        if self.x < self.x_min {
            self.x_min = self.x;
        }
        if self.y < self.y_min {
            self.y_min = self.y;
        }
        return self;
    }
    fn max(&mut self) -> &mut Position2 {
        if self.x > self.x_max {
            self.x_max = self.x;
        }
        if self.y > self.y_max {
            self.y_max = self.y;
        }
        return self;
    }
}

pub fn part2(program: &Vec<isize>) -> i64 {
    // for knowing where on teh grid I am
    let mut position = Position2 { x: 0, y: 0, x_min: 0, y_min: 0, x_max: 0, y_max: 0 };
    let mut direction = 1;

    let mut grid = Grid::new();

    // set 0,0 to white
    let inner = grid.grid.entry(0).or_insert(HashMap::new());
    inner.entry(0).or_insert(Cell { colour: 1, painted: 0 });

    // boot up teh computer
    let mut computer = Computer::new();
    computer.set_program(program);

    loop {
        // if it finishes teh program
        if computer.finished {
            break;
        }

        let colour = grid.get_colour(&position.x, &position.y) as isize;
        computer.set_input(colour).run();

        let output = &computer.output;

        grid.set_colour(position.x, position.y, output[0] as i64);

        // get new direction
        match output[1] {
            0 => {
                if direction == 1 {
                    direction = 4
                } else {
                    direction -= 1
                }
            }
            1 => {
                if direction == 4 {
                    direction = 1
                } else {
                    direction += 1
                }
            }
            // should not happen
            _ => {}
        }

        // get new position
        match direction {
            // north
            1 => position.y += 1,
            // east
            2 => position.x += 1,
            // south
            3 => position.y -= 1,
            // west
            4 => position.x -= 1,
            // should not happen
            _ => {}
        }

        // organuise min/max here
        position.min().max();

        computer.clear_output();
    }

    draw_identifier(&grid, &position);

    0
}

fn draw_identifier(grid: &Grid, position: &Position2) {
    // position gives the min/max for the graph
    for y in (position.y_min..=position.y_max).rev() {
        let mut line = "".to_string();
        for x in position.x_min..position.x_max {
            match grid.get_colour(&x, &y) {
                // black
                0 => {
                    line += " ";
                }
                // white
                1 => {
                    line += "#";
                }
                _ => {}
            }
        }
        println!("{}", line);
    }
}

#[allow(dead_code)]
pub fn testing(_input: &Vec<isize>) -> isize {
    //let input1 = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
    //let input1 = "1102,34915192,34915192,7,4,7,99,0";
    //let input1 = "104,1125899906842624,99";

    //let generator1 = generator(input1);

    let mut computer = Computer::new();
    computer.set_program(_input);
    computer.set_input(1).run();

    //computer.set_input(1).run();
    //computer.run();
    println!("Output: {:?} input_waiting: {}", computer.output, computer.input_waiting);

    /*
    process_opcode (&ProcessOPCodeIn{
        program: generator1,
        noun: None,
        verb: None,
        input: None,
        pointer: None,
        relative_base: None
    });

     */

    0
}
