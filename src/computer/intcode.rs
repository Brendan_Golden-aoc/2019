pub mod day02 {
    pub fn process_opcode(input: &Vec<i32>, noun: i32, verb: i32) -> i32 {
        let mut working_vec = input.to_vec();

        // making initial replacements
        working_vec[1] = noun;
        working_vec[2] = verb;

        for position in 0..working_vec.len() {
            // align with teh opcode
            if position % 4 != 0 {
                continue;
            }

            match working_vec[position] {
                1 => {
                    let position1 = working_vec[position + 1] as usize;
                    let position2 = working_vec[position + 2] as usize;

                    let placement = working_vec[position + 3] as usize;

                    working_vec[placement] = working_vec[position1] + working_vec[position2];
                }
                2 => {
                    let position1 = working_vec[position + 1] as usize;
                    let position2 = working_vec[position + 2] as usize;

                    let placement = working_vec[position + 3] as usize;

                    working_vec[placement] = working_vec[position1] * working_vec[position2];
                }
                99 => break,
                _ => continue,
            }
        }

        working_vec[0]
    }
}

pub mod day05 {
    pub fn process_opcode(program: &Vec<isize>, noun: Option<isize>, verb: Option<isize>, input: isize) -> isize {
        let mut working_vec: Vec<isize> = program.to_vec();

        // making initial replacements
        match noun {
            Some(noun_value) => working_vec[1] = noun_value,
            None => {}
        }
        match verb {
            Some(verb_value) => working_vec[2] = verb_value,
            None => {}
        }

        // in/out
        let mut output: isize = 0;

        let mut pointer: usize = 0;
        loop {
            let op_mode = process_opmode(working_vec[pointer]);
            let values = process_values(&working_vec, &pointer, &op_mode);

            //println!("{:?} ", working_vec);
            //println!("{:?} {:?} ", op_mode, values);
            match op_mode.operation.as_str() {
                // sum and store
                "01" => {
                    let placement = working_vec[pointer + 3] as usize;
                    working_vec[placement] = values.value_1.unwrap() + values.value_2.unwrap();

                    pointer += 4;
                }
                // multiply and store
                "02" => {
                    let placement = working_vec[pointer + 3] as usize;
                    working_vec[placement] = values.value_1.unwrap() * values.value_2.unwrap();

                    pointer += 4;
                }
                // take teh imput value and store at the location
                "03" => {
                    let placement = working_vec[pointer + 1] as usize;
                    working_vec[placement] = input;

                    pointer += 2;
                }
                // take teh value at the position and output it
                "04" => {
                    println!("Diagnostic: {}", values.value_1.unwrap());

                    output = values.value_1.unwrap();

                    pointer += 2;
                }
                // jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing
                "05" => {
                    if values.value_1.unwrap() != 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }

                // jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                "06" => {
                    if values.value_1.unwrap() == 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }

                // if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "07" => {
                    let placement = working_vec[pointer + 3] as usize;
                    if values.value_1.unwrap() < values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }

                // if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "08" => {
                    let placement = working_vec[pointer + 3] as usize;
                    if values.value_1.unwrap() == values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }

                // end program
                "99" => break,
                // handle all other numbers
                _ => {
                    pointer += 1;
                }
            }
        }

        output
    }

    #[derive(Debug)]
    pub struct OPMode {
        pub operation: String,
        pub mode_1: u32,
        pub mode_2: u32,
        pub mode_3: u32,
    }
    pub fn process_opmode(value: isize) -> OPMode {
        let split = value.to_string().chars().map(|d| d.to_digit(10).unwrap_or(0)).collect::<Vec<u32>>();

        let operation_0;
        let operation_1;
        match split.len().checked_sub(2) {
            Some(v) => {
                operation_0 = split[v];
            }
            None => {
                operation_0 = 0;
            }
        }
        match split.len().checked_sub(1) {
            Some(v) => {
                operation_1 = split[v];
            }
            None => {
                operation_1 = 0;
            }
        }
        let operation = format!("{}{}", operation_0, operation_1);

        let mode_1;
        match split.len().checked_sub(3) {
            Some(v) => {
                mode_1 = split[v];
            }
            None => {
                mode_1 = 0;
            }
        }
        let mode_2;
        match split.len().checked_sub(4) {
            Some(v) => {
                mode_2 = split[v];
            }
            None => {
                mode_2 = 0;
            }
        }
        let mode_3;
        match split.len().checked_sub(5) {
            Some(v) => {
                mode_3 = split[v];
            }
            None => {
                mode_3 = 0;
            }
        }

        OPMode { operation, mode_1, mode_2, mode_3 }
    }

    #[derive(Debug)]
    pub struct Values {
        pub value_1: Option<isize>,
        pub value_2: Option<isize>,
        pub value_3: Option<isize>,
    }
    pub fn process_values(working_vec: &Vec<isize>, position: &usize, op_mode: &OPMode) -> Values {
        let value_1 = process_values_sub(working_vec, position, op_mode.mode_1, 1);
        let value_2 = process_values_sub(working_vec, position, op_mode.mode_2, 2);
        let value_3 = process_values_sub(working_vec, position, op_mode.mode_3, 3);

        Values { value_1, value_2, value_3 }
    }
    fn process_values_sub(working_vec: &Vec<isize>, position: &usize, op_mode: u32, offset: usize) -> Option<isize> {
        //println!("Position: {} Offset: {} Mode: {} Length: {}", position, offset, op_mode, working_vec.len());
        let value: Option<isize>;
        if check_index(working_vec, position + offset) {
            match op_mode {
                0 => {
                    let new_index = working_vec[position + offset] as usize;
                    if check_index(working_vec, new_index) {
                        value = Some(working_vec[new_index])
                    } else {
                        value = None;
                    }
                    //println!("Mode: {} value: {:?} new_index: {}", op_mode, value, new_index);
                }
                1 => value = Some(working_vec[position + offset]),
                _ => {
                    value = None;
                }
            }
        } else {
            value = None;
        }

        //println!("Mode: {} value: {:?} offset: {}", op_mode, value, offset);
        value
    }
    fn check_index(working_vec: &Vec<isize>, index: usize) -> bool {
        let index = index as isize;
        let max_len = working_vec.len() as isize;
        if (index >= 0) && (index < max_len) {
            true
        } else {
            false
        }
    }
}

pub mod day07 {
    use super::day05::{process_opmode, process_values};

    #[derive(Debug)]
    pub struct OPCodeResult {
        pub diagnostic: isize,
        pub first: isize,
        pub working_vec: Vec<isize>,
        pub halted: bool,
        pub pointer: usize,
    }
    pub fn process_opcode(program: &Vec<isize>, noun: Option<isize>, verb: Option<isize>, input: Vec<isize>, pointer_old: Option<usize>) -> OPCodeResult {
        let mut working_vec: Vec<isize> = program.to_vec();

        // making initial replacements
        match noun {
            Some(noun_value) => working_vec[1] = noun_value,
            None => {}
        }
        match verb {
            Some(verb_value) => working_vec[2] = verb_value,
            None => {}
        }

        let mut input_used = vec![false; input.len()];

        // in/out
        let mut diagnostic: isize = 0;

        let mut halted = false;

        let mut pointer: usize = 0;
        match pointer_old {
            Some(pointer_value) => pointer = pointer_value,
            None => {}
        }

        loop {
            let op_mode = process_opmode(working_vec[pointer]);
            let values = process_values(&working_vec, &pointer, &op_mode);

            //println!("{:?} ", working_vec);
            //println!("{:?} {:?} ", op_mode, values);
            match op_mode.operation.as_str() {
                // sum and store
                "01" => {
                    let placement = working_vec[pointer + 3] as usize;
                    working_vec[placement] = values.value_1.unwrap() + values.value_2.unwrap();

                    pointer += 4;
                }
                // multiply and store
                "02" => {
                    let placement = working_vec[pointer + 3] as usize;
                    working_vec[placement] = values.value_1.unwrap() * values.value_2.unwrap();

                    pointer += 4;
                }
                // take teh imput value and store at the location
                "03" => {
                    let placement = working_vec[pointer + 1] as usize;

                    for (index, used) in input_used.iter().enumerate() {
                        if !used {
                            working_vec[placement] = input[index];
                            input_used[index] = true;
                            break;
                        }
                    }

                    pointer += 2;
                }
                // take teh value at the position and output it
                "04" => {
                    //println!("Diagnostic: {}", values.value_1.unwrap());

                    diagnostic = values.value_1.unwrap();

                    pointer += 2;

                    if diagnostic != 0 {
                        break;
                    }
                }
                // jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing
                "05" => {
                    if values.value_1.unwrap() != 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }

                // jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                "06" => {
                    if values.value_1.unwrap() == 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }

                // if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "07" => {
                    let placement = working_vec[pointer + 3] as usize;
                    if values.value_1.unwrap() < values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }

                // if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "08" => {
                    let placement = working_vec[pointer + 3] as usize;
                    if values.value_1.unwrap() == values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }

                // end program
                "99" => {
                    halted = true;
                    break;
                }
                // handle all other numbers
                _ => {
                    pointer += 1;
                }
            }
        }

        OPCodeResult { diagnostic, first: working_vec[0], working_vec, halted, pointer }
    }
}

pub mod day09 {
    use super::day05::{process_opmode, OPMode};

    #[derive(Debug, Clone)]
    pub struct ProcessOPCodeIn {
        pub program: Vec<isize>,
        pub noun: Option<isize>,
        pub verb: Option<isize>,
        pub input: Option<Vec<isize>>,
        pub pointer: Option<usize>,
        pub relative_base: Option<isize>,
    }

    #[derive(Debug)]
    pub struct ProcessOPCodeOut {
        pub output: isize,
        pub first: isize,
        pub program: Vec<isize>,
        pub halted: bool,
        pub pointer: usize,
        pub relative_base: isize,
    }
    pub fn process_opcode(input: &ProcessOPCodeIn) -> Option<ProcessOPCodeOut> {
        let input = input.to_owned();
        let mut working_vec = input.program.to_vec();

        let input_values: Vec<isize>;
        let mut input_used: Vec<bool>;
        let mut pointer: usize;
        let mut relative_base: isize;

        match input.noun {
            Some(noun) => working_vec[1] = noun,
            None => {}
        }
        match input.verb {
            Some(verb) => working_vec[2] = verb,
            None => {}
        }
        match input.input {
            Some(input_new) => {
                input_values = input_new;
                input_used = vec![false; input_values.len()];
            }
            None => {
                input_values = vec![];
                input_used = vec![];
            }
        }
        match input.pointer {
            Some(pointer_new) => pointer = pointer_new,
            None => pointer = 0,
        }
        match input.relative_base {
            Some(relative_base_new) => relative_base = relative_base_new,
            None => relative_base = 0,
        }

        // local var only
        let mut diagnostic: isize = 0;
        let mut halted = false;

        loop {
            let op_mode = process_opmode(working_vec[pointer]);
            let values = process_values(&working_vec, &pointer, &op_mode, relative_base);

            //println!("op_mode: {:?} values: {:?}  relative_base:{}", op_mode, values,  relative_base);

            match op_mode.operation.as_str() {
                // sum and store
                "01" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((working_vec[pointer + 3] as isize) + (relative_base as isize)) as usize;
                    } else {
                        placement = working_vec[pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    working_vec = extend_working_vec(&working_vec, max);

                    working_vec[placement] = values.value_1.unwrap() + values.value_2.unwrap();

                    pointer += 4;
                }
                // multiply and store
                "02" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((working_vec[pointer + 3] as isize) + (relative_base as isize)) as usize;
                    } else {
                        placement = working_vec[pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    working_vec = extend_working_vec(&working_vec, max);

                    working_vec[placement] = values.value_1.unwrap() * values.value_2.unwrap();

                    pointer += 4;
                }
                // take teh imput value and store at the location
                "03" => {
                    // let placement = working_vec[pointer +1] as usize
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_1 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((working_vec[pointer + 1] as isize) + (relative_base as isize)) as usize;
                    } else {
                        placement = working_vec[pointer + 1] as usize
                    }

                    let mut max = 0;
                    if placement > max {
                        max = placement
                    }

                    working_vec = extend_working_vec(&working_vec, max);

                    for (index, used) in input_used.iter().enumerate() {
                        if !used {
                            working_vec[placement] = input_values[index];
                            if input_used.len() != 1 {
                                input_used[index] = true;
                            }

                            break;
                        }
                    }

                    pointer += 2;
                }
                // take teh value at the position and output it
                "04" => {
                    println!("Diagnostic: {}", values.value_1.unwrap());

                    diagnostic = values.value_1.unwrap();

                    pointer += 2;
                }
                // jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing
                "05" => {
                    if values.value_1.unwrap() != 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }
                // jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                "06" => {
                    if values.value_1.unwrap() == 0 {
                        pointer = values.value_2.unwrap() as usize;
                    } else {
                        pointer += 3;
                    }
                }
                // if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "07" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((working_vec[pointer + 3] as isize) + (relative_base as isize)) as usize;
                    } else {
                        placement = working_vec[pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    working_vec = extend_working_vec(&working_vec, max);

                    if values.value_1.unwrap() < values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }
                // if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "08" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((working_vec[pointer + 3] as isize) + (relative_base as isize)) as usize;
                    } else {
                        placement = working_vec[pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    //println!("08 Max: {} placement:{} rawpalcement: {} sum:{}", max, placement, working_vec[pointer +3], (working_vec[pointer +3] as isize) + (relative_base as isize) );

                    working_vec = extend_working_vec(&working_vec, max);

                    if values.value_1.unwrap() == values.value_2.unwrap() {
                        working_vec[placement] = 1;
                    } else {
                        working_vec[placement] = 0;
                    }
                    pointer += 4;
                }
                // adjusts the relative base by the value of its only parameter. The relative base increases (or decreases, if the value is negative) by the value of the parameter.
                "09" => {
                    relative_base += values.value_1.unwrap();
                    pointer += 2;

                    //break
                }
                // end program
                "99" => {
                    halted = true;
                    break;
                }
                // handle all other numbers
                _ => {
                    pointer += 1;
                }
            }
        }

        Some(ProcessOPCodeOut { output: diagnostic, first: working_vec[0], program: working_vec, halted, pointer, relative_base })
    }

    #[derive(Debug)]
    pub struct Values {
        pub value_1: Option<isize>,
        pub value_2: Option<isize>,
        pub value_3: Option<isize>,
        pub max_1: usize,
        pub max_2: usize,
        pub max_3: usize,
    }
    pub fn process_values(working_vec: &Vec<isize>, position: &usize, op_mode: &OPMode, relative_base: isize) -> Values {
        let (value_1, max_1) = process_values_sub(working_vec, op_mode.mode_1, position, 1, relative_base);
        let (value_2, max_2) = process_values_sub(working_vec, op_mode.mode_2, position, 2, relative_base);
        let (value_3, max_3) = process_values_sub(working_vec, op_mode.mode_3, position, 3, relative_base);

        Values { value_1, value_2, value_3, max_1, max_2, max_3 }
    }
    fn process_values_sub(working_vec: &Vec<isize>, op_mode: u32, position: &usize, offset: usize, relative_base: isize) -> (Option<isize>, usize) {
        // initial vars
        let current_max_len = working_vec.len();
        let offset_position = position + offset;
        // what we are returning
        let mut index_max = offset_position;
        let value: Option<isize>;

        if offset_position >= 0 {
            if offset_position >= current_max_len {
                return (Some(0), index_max);
            }

            match op_mode {
                // positon mode, which causes the parameter to be interpreted as a position
                0 => {
                    let index_new = working_vec[offset_position] as usize;

                    if index_new > index_max {
                        index_max = index_new
                    }
                    if index_new >= 0 {
                        if index_new >= current_max_len {
                            // handle stuff out of range
                            value = Some(0)
                        } else {
                            //println!("offset_position: {} index_new:{}", offset_position, index_new);
                            value = Some(working_vec[index_new])
                        }
                    } else {
                        value = None;
                    }
                }
                // immediate mode, In immediate mode, a parameter is interpreted as a value
                1 => value = Some(working_vec[offset_position]),
                // relative mode, which causes the parameter to be interpreted as a position + relative_base
                2 => {
                    //println!("{} {}", working_vec[offset_position], relative_base);
                    let index_new = (working_vec[offset_position] + relative_base) as usize;

                    if index_new > index_max {
                        index_max = index_new
                    }

                    if index_new >= 0 {
                        if index_new >= current_max_len {
                            // handle stuff out of range
                            value = Some(0)
                        } else {
                            value = Some(working_vec[index_new])
                        }
                    } else {
                        value = None;
                    }
                }

                _ => {
                    value = None;
                }
            }
        } else {
            value = None;
        }

        //println!("Mode: {} value: {:?} offset: {}", op_mode, value, offset);
        (value, index_max)
    }

    pub fn extend_working_vec(working_vec: &Vec<isize>, new_len: usize) -> Vec<isize> {
        let mut tmp_array = working_vec.to_owned();
        let initial_len = tmp_array.len();

        if initial_len > new_len {
            return tmp_array;
        }

        let tmp_addition: Vec<isize> = vec![0; (new_len - initial_len) + 2];
        //println!("Max: {}  Vec: {}, Vec+: {}", new_len, initial_len, tmp_addition.len());
        tmp_array.extend(tmp_addition.iter().cloned());

        tmp_array
    }
}

pub mod day11 {
    use super::day05::{process_opmode, OPMode};
    use super::day09::{extend_working_vec, process_values, Values};

    #[derive(Debug)]
    pub struct Computer {
        pub program: Vec<isize>,
        pointer: usize,
        relative_base: isize,

        // theser relate to the results
        pub input: Option<isize>,
        pub output: Vec<isize>,

        // these are for processing
        pub finished: bool,
        pub input_waiting: bool,
        diagnostic: bool,
    }

    impl Computer {
        pub fn new() -> Computer {
            return Computer { program: vec![], pointer: 0, relative_base: 0, output: vec![], input: None, finished: false, input_waiting: false, diagnostic: false };
        }

        pub fn set_program(&mut self, program: &Vec<isize>) -> &mut Computer {
            self.program = program.to_vec();
            return self;
        }
        pub fn set_noun(&mut self, noun: isize) -> &mut Computer {
            self.program[1] = noun;
            return self;
        }
        pub fn set_verb(&mut self, verb: isize) -> &mut Computer {
            self.program[2] = verb;
            return self;
        }
        pub fn set_value(&mut self, position: usize, value: isize) -> &mut Computer {
            self.program[position] = value;
            return self;
        }

        pub fn set_input(&mut self, input: isize) -> &mut Computer {
            // set input
            self.input = Some(input);
            // clear blocking flag
            self.input_waiting = false;

            // resume loop?

            return self;
        }

        pub fn set_diagnostic(&mut self, diagnostic: bool) -> &mut Computer {
            self.diagnostic = diagnostic;
            return self;
        }

        pub fn clear_output(&mut self) -> &mut Computer {
            self.output = vec![];
            return self;
        }

        pub fn run(&mut self) -> &mut Computer {
            loop {
                // handle stopped cases
                if self.input_waiting == true {
                    break;
                }
                if self.finished == true {
                    break;
                }

                let op_mode = process_opmode(self.program[self.pointer]);
                let values = process_values(&self.program, &self.pointer, &op_mode, self.relative_base);

                self.process_command(op_mode, values);
            }

            return self;
        }

        fn process_command(&mut self, op_mode: OPMode, values: Values) {
            match op_mode.operation.as_str() {
                // sum and store
                "01" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((self.program[self.pointer + 3] as isize) + (self.relative_base as isize)) as usize;
                    } else {
                        placement = self.program[self.pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    self.program = extend_working_vec(&self.program, max);

                    self.program[placement] = values.value_1.unwrap() + values.value_2.unwrap();

                    self.pointer += 4;
                }
                // multiply and store
                "02" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((self.program[self.pointer + 3] as isize) + (self.relative_base as isize)) as usize;
                    } else {
                        placement = self.program[self.pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    self.program = extend_working_vec(&self.program, max);

                    self.program[placement] = values.value_1.unwrap() * values.value_2.unwrap();

                    self.pointer += 4;
                }
                // take teh imput value and store at the location
                "03" => {
                    let input;
                    match self.input {
                        Some(input_value) => {
                            // pull out the input
                            input = input_value;
                            // set its value to none
                            self.input = None;
                        }
                        None => {
                            self.input_waiting = true;
                            return;
                        }
                    }

                    // let placement = working_vec[pointer +1] as usize
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_1 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((self.program[self.pointer + 1] as isize) + (self.relative_base as isize)) as usize;
                    } else {
                        placement = self.program[self.pointer + 1] as usize
                    }

                    let mut max = 0;
                    if placement > max {
                        max = placement
                    }

                    self.program = extend_working_vec(&self.program, max);

                    self.program[placement] = input;

                    self.pointer += 2;
                }
                // take teh value at the position and output it
                "04" => {
                    let output = values.value_1.unwrap();

                    if self.diagnostic {
                        println!("Diagnostic: {}", output);
                    }

                    self.output.push(output);
                    self.pointer += 2;
                }
                // jump-if-true: if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing
                "05" => {
                    if values.value_1.unwrap() != 0 {
                        self.pointer = values.value_2.unwrap() as usize;
                    } else {
                        self.pointer += 3;
                    }
                }
                // jump-if-false: if the first parameter is zero, it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
                "06" => {
                    if values.value_1.unwrap() == 0 {
                        self.pointer = values.value_2.unwrap() as usize;
                    } else {
                        self.pointer += 3;
                    }
                }
                // if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "07" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((self.program[self.pointer + 3] as isize) + (self.relative_base as isize)) as usize;
                    } else {
                        placement = self.program[self.pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    self.program = extend_working_vec(&self.program, max);

                    if values.value_1.unwrap() < values.value_2.unwrap() {
                        self.program[placement] = 1;
                    } else {
                        self.program[placement] = 0;
                    }
                    self.pointer += 4;
                }
                // if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
                "08" => {
                    let placement: usize;
                    //let placement = values.value_1.unwrap() as usize;

                    if op_mode.mode_3 == 2 {
                        // = values.value_1.unwrap()
                        placement = ((self.program[self.pointer + 3] as isize) + (self.relative_base as isize)) as usize;
                    } else {
                        placement = self.program[self.pointer + 3] as usize
                    }

                    let mut max = 0;
                    if values.max_1 > max {
                        max = values.max_1
                    }
                    if values.max_2 > max {
                        max = values.max_2
                    }
                    if placement > max {
                        max = placement
                    }

                    //println!("08 Max: {} placement:{} rawpalcement: {} sum:{}", max, placement, working_vec[pointer +3], (working_vec[pointer +3] as isize) + (relative_base as isize) );

                    self.program = extend_working_vec(&self.program, max);

                    if values.value_1.unwrap() == values.value_2.unwrap() {
                        self.program[placement] = 1;
                    } else {
                        self.program[placement] = 0;
                    }
                    self.pointer += 4;
                }
                // adjusts the relative base by the value of its only parameter. The relative base increases (or decreases, if the value is negative) by the value of the parameter.
                "09" => {
                    self.relative_base += values.value_1.unwrap();
                    self.pointer += 2;
                }
                // end program
                "99" => {
                    self.finished = true;
                }
                // handle all other numbers
                _ => {
                    self.pointer += 1;
                }
            }
        }
    }
}
