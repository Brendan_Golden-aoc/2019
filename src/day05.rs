use super::computer::intcode::day05::process_opcode;

pub fn generator(input: &str) -> Vec<isize> {
    let mut result: Vec<isize> = Vec::new();

    for s in input.split(",") {
        result.push(s.parse::<isize>().unwrap())
    }

    result
}

pub fn part1(input: &Vec<isize>) -> isize {
    process_opcode(input, None, None, 1)
}

pub fn part2(input: &Vec<isize>) -> isize {
    process_opcode(input, None, None, 5)
}

#[allow(dead_code)]
pub fn testing(_input: &Vec<isize>) -> i32 {
    println!("Testing");
    //*
    let input1 = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";

    let generator1 = generator(input1);
    let result1_1 = process_opcode(&generator1, None, None, 7);

    println!("Testing - 1_1 - 999 {}", result1_1);

    let result1_2 = process_opcode(&generator1, None, None, 8);
    println!("Testing - 1_2 - 1000 {}", result1_2);

    let result1_3 = process_opcode(&generator1, None, None, 9);
    println!("Testing - 1_3 - 1001 {}", result1_3);
    // */
    let input2 = "3,9,8,9,10,9,4,9,99,-1,8";

    let generator2 = generator(input2);

    let result2_1 = process_opcode(&generator2, None, None, 7);
    println!("Testing - 2_1 - 0 {}", result2_1);

    let result2_2 = process_opcode(&generator2, None, None, 8);
    println!("Testing - 2_1 - 1 {}", result2_2);

    println!("Testing");
    0
}
