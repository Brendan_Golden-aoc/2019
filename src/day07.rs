use super::computer::intcode::day07::process_opcode;

pub fn generator(input: &str) -> Vec<isize> {
    let mut result: Vec<isize> = Vec::new();

    for s in input.split(",") {
        result.push(s.parse::<isize>().unwrap())
    }

    result
}

pub fn part1(input: &Vec<isize>) -> isize {
    let sequences = generate_sequence(0, 4);

    let mut max_thrust = 0;

    for sequence in sequences {
        // A
        let a = process_opcode(input, None, None, vec![sequence[0], 0], None).diagnostic;
        // B
        let b = process_opcode(input, None, None, vec![sequence[1], a], None).diagnostic;
        // C
        let c = process_opcode(input, None, None, vec![sequence[2], b], None).diagnostic;
        // D
        let d = process_opcode(input, None, None, vec![sequence[3], c], None).diagnostic;
        // E
        let e = process_opcode(input, None, None, vec![sequence[4], d], None).diagnostic;

        if e > max_thrust {
            max_thrust = e;
        }
    }

    max_thrust
}

fn generate_sequence(min: isize, max: isize) -> Vec<[isize; 5]> {
    let mut sequence_list: Vec<[isize; 5]> = Vec::new();

    for x_1 in min..=max {
        for x_2 in min..=max {
            if x_2 == x_1 {
                continue;
            }
            for x_3 in min..=max {
                if x_3 == x_1 {
                    continue;
                }
                if x_3 == x_2 {
                    continue;
                }
                for x_4 in min..=max {
                    if x_4 == x_1 {
                        continue;
                    }
                    if x_4 == x_2 {
                        continue;
                    }
                    if x_4 == x_3 {
                        continue;
                    }
                    for x_5 in min..=max {
                        if x_5 == x_1 {
                            continue;
                        }
                        if x_5 == x_2 {
                            continue;
                        }
                        if x_5 == x_3 {
                            continue;
                        }
                        if x_5 == x_4 {
                            continue;
                        }
                        sequence_list.push([x_1, x_2, x_3, x_4, x_5]);
                    }
                }
            }
        }
    }

    sequence_list
}

pub fn part2(input: &Vec<isize>) -> isize {
    let sequences = generate_sequence(5, 9);

    let mut max_thrust = 0;

    for sequence in sequences {
        // first round
        // A
        let a = process_opcode(input, None, None, vec![sequence[0], 0], None);
        // B
        let b = process_opcode(input, None, None, vec![sequence[1], a.diagnostic], None);
        // C
        let c = process_opcode(input, None, None, vec![sequence[2], b.diagnostic], None);
        // D
        let d = process_opcode(input, None, None, vec![sequence[3], c.diagnostic], None);
        // E
        let e = process_opcode(input, None, None, vec![sequence[4], d.diagnostic], None);

        // now loop until e.halted == true

        let mut a_1 = a;
        let mut b_1 = b;
        let mut c_1 = c;
        let mut d_1 = d;
        let mut e_1 = e;

        loop {
            a_1 = process_opcode(&a_1.working_vec, None, None, vec![e_1.diagnostic], Some(a_1.pointer));
            // B
            b_1 = process_opcode(&b_1.working_vec, None, None, vec![a_1.diagnostic], Some(b_1.pointer));
            // C
            c_1 = process_opcode(&c_1.working_vec, None, None, vec![b_1.diagnostic], Some(c_1.pointer));
            // D
            d_1 = process_opcode(&d_1.working_vec, None, None, vec![c_1.diagnostic], Some(d_1.pointer));
            // E
            e_1 = process_opcode(&e_1.working_vec, None, None, vec![d_1.diagnostic], Some(e_1.pointer));

            if e_1.diagnostic > max_thrust {
                max_thrust = e_1.diagnostic;
            }
            if e_1.halted == true {
                break;
            }
        }
    }

    max_thrust
}

#[allow(dead_code)]
pub fn testing(_input: &Vec<isize>) -> isize {
    let input1 = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5";
    //let input1 = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10";

    let generator1 = generator(input1);

    part2(&generator1);

    0
}
