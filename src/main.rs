mod computer;
mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day07;
mod day09;
mod day11;
mod day13;

aoc::main! {
    year 2019;
    day01 : input_generator => solve_part1, solve_part2;
    day02 : input_generator => solve_part1, solve_part2?;
    day03 : input_generator => solve_part1, solve_part2;
    day04 : input_generator => solve_part1, solve_part2;
    day05 : generator => part1, part2;
    day07 : generator => part1, part2;
    day09 : generator => part1, part2;
    day11 : generator => part1, part2;
    day13 : generator => part1, part2;
}
