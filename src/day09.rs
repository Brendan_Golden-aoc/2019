use super::computer::intcode::day09::{process_opcode, ProcessOPCodeIn};

pub fn generator(input: &str) -> Vec<isize> {
    let mut result: Vec<isize> = Vec::new();

    for s in input.split(",") {
        result.push(s.parse::<isize>().unwrap())
    }

    result
}

#[allow(dead_code)]
pub fn testing(_input: &Vec<isize>) -> isize {
    //let input1 = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
    //let input1 = "1102,34915192,34915192,7,4,7,99,0";
    let input1 = "104,1125899906842624,99";

    let generator1 = generator(input1);

    process_opcode(&ProcessOPCodeIn { program: generator1, noun: None, verb: None, input: None, pointer: None, relative_base: None });

    0
}

pub fn part1(input: &Vec<isize>) -> isize {
    let result = process_opcode(&ProcessOPCodeIn { program: input.to_owned(), noun: None, verb: None, input: Some(vec![1]), pointer: None, relative_base: None });

    return match result {
        Some(result_processed) => result_processed.output,
        None => 0,
    };
}

pub fn part2(input: &Vec<isize>) -> isize {
    let result = process_opcode(&ProcessOPCodeIn { program: input.to_owned(), noun: None, verb: None, input: Some(vec![2]), pointer: None, relative_base: None });

    return match result {
        Some(result_processed) => result_processed.output,
        None => 0,
    };
}
