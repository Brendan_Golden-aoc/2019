pub fn input_generator(input: &str) -> (i32, i32) {
    let values: Vec<i32> = input.split("-").map(|x| x.parse::<i32>().unwrap()).collect();

    (values[0], values[1])
}

pub fn solve_part1(input: &(i32, i32)) -> i32 {
    let mut result: Vec<i32> = Vec::new();

    let start = input.0;
    let end = input.1;

    for number in start..end {
        let mut double = false;
        let mut increased = true;

        // initial out of scope, gets overwritten
        let mut last = -1;
        for (i, c) in number.to_string().chars().enumerate() {
            let converted: i32 = c.to_digit(10).unwrap() as i32;
            if i != 0 {
                if converted < last {
                    increased = false;
                }
                if converted == last {
                    double = true;
                }
            }
            last = converted
        }

        if !double {
            continue;
        }
        if !increased {
            continue;
        }

        result.push(number)
    }

    result.len() as i32
}

pub fn solve_part2(input: &(i32, i32)) -> i32 {
    let mut result: Vec<i32> = Vec::new();

    let start = input.0;
    let end = input.1;

    for number in start..end {
        let mut increased = true;
        let mut double_len = 1;
        let mut double_valid = 0;

        // initial out of scope, gets overwritten
        let mut last = -1;
        for (i, c) in number.to_string().chars().enumerate() {
            let converted: i32 = c.to_digit(10).unwrap() as i32;
            if i != 0 {
                if converted < last {
                    increased = false;
                }
                if converted == last {
                    double_len += 1;
                }
                if converted > last {
                    // if the double was of length 2 (a double) its valid
                    if double_len == 2 {
                        double_valid += 1;
                    }

                    // reset the double count
                    double_len = 1
                }
            }
            last = converted
        }

        // catch any tail end
        if double_len == 2 {
            double_valid += 1;
        }

        if double_valid == 0 {
            continue;
        }
        if !increased {
            continue;
        }

        result.push(number)
    }

    result.len() as i32
}
