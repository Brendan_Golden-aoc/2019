use super::computer::intcode::day02::process_opcode;

// first time doing opcode, fun!

pub fn input_generator(input: &str) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();

    for s in input.split(",") {
        result.push(s.parse::<i32>().unwrap())
    }

    result
}

pub fn solve_part1(input: &Vec<i32>) -> i32 {
    process_opcode(input, 12, 2)
}

pub fn solve_part2(input: &Vec<i32>) -> Option<i32> {
    for i in 0..99 {
        for j in 0..99 {
            if process_opcode(input, i, j) == 19690720 {
                return Some((100 * i) + j);
            }
        }
    }

    None
}
