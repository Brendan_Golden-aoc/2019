use std::cmp::{max, min};

#[derive(Debug)]
pub struct Line {
    pub x1: i32,
    pub y1: i32,
    pub x2: i32,
    pub y2: i32,
    pub direction: String,
    pub distance: i32,
}

#[derive(Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
    pub distance: i32,
    pub distance_line: i32,
}

pub fn input_generator(input: &str) -> (Vec<Line>, Vec<Line>) {
    let lines: Vec<&str> = input.split("\n").collect();

    let line1: Vec<&str> = lines[0].split(",").collect();
    let line2: Vec<&str> = lines[1].split(",").collect();

    (get_lines(line1), get_lines(line2))
}

fn get_lines(commands: Vec<&str>) -> Vec<Line> {
    let mut result: Vec<Line> = Vec::new();

    let mut position_x = 0;
    let mut position_y = 0;
    let mut distance_travelled = 0;

    for command in commands {
        let direction = &command[..1];
        let distance = &command[1..].parse::<i32>().unwrap();

        // set initial values
        let mut line = Line {
            x1: position_x,
            y1: position_y,
            x2: 0,
            y2: 0,
            direction: "".to_string(),
            // distance to the start of teh line (x1,y1)
            distance: distance_travelled,
        };

        // x = left/right
        // y = up/down
        match direction {
            "R" => {
                position_x += distance;
                line.direction = "h".to_string()
            }
            "L" => {
                position_x -= distance;
                line.direction = "h".to_string()
            }
            "U" => {
                position_y += distance;
                line.direction = "v".to_string()
            }
            "D" => {
                position_y -= distance;
                line.direction = "v".to_string()
            }
            _ => {
                println!("{} {}", direction, distance);
                continue;
            }
        }

        // set new values
        line.x2 = position_x;
        line.y2 = position_y;

        distance_travelled += distance;

        // pop it into teh vector
        result.push(line);
    }

    result
}

pub fn solve_part1(input: &(Vec<Line>, Vec<Line>)) -> i32 {
    let intersections = get_intersections(&input.0, &input.1);

    let min_distance = get_min_distance(&intersections);

    min_distance
}

pub fn solve_part2(input: &(Vec<Line>, Vec<Line>)) -> i32 {
    let intersections = get_intersections(&input.0, &input.1);

    let min_distance = get_min_distance_wires(&intersections);

    min_distance
}

fn get_intersections(lines1: &Vec<Line>, lines2: &Vec<Line>) -> Vec<Point> {
    let mut result: Vec<Point> = Vec::new();

    for line1 in lines1 {
        for line2 in lines2 {
            // check if perpendicular

            // on teh same line, toss it out
            if line1.direction == line2.direction {
                continue;
            }

            // theya re perpendicular

            let vert;
            let hor;
            if line1.direction == "v".to_string() {
                vert = line1;
                hor = line2;
            } else {
                vert = line2;
                hor = line1;
            }

            // get the point of intersection if they are infinite
            let point = Point {
                x: vert.x1,
                y: hor.y1,
                // cabby distance
                distance: vert.x1.abs() + hor.y1.abs(),

                // distance following the lines to this point
                //distance_line: (vert.distance /* + max(vert.y1, hor.y1)  vert.y1 to hor.y1 */) + (hor.distance /* + hor.x1 to vert.x1 */)
                distance_line: (vert.distance + (max(vert.y1, hor.y1) - min(vert.y1, hor.y1))) + (hor.distance + (max(hor.x1, vert.x1) - min(hor.x1, vert.x1))),
            };

            // check if point is on both lines
            // vert

            // y_point has to between vert.y1 and vert.y2
            let vert_range = min(vert.y1, vert.y2)..max(vert.y1, vert.y2);
            if !vert_range.contains(&point.y) {
                continue;
            }

            let hor_range = min(hor.x1, hor.x2)..max(hor.x1, hor.x2);
            if !hor_range.contains(&point.x) {
                continue;
            }

            result.push(point)
        }
    }

    result
}

fn get_min_distance(points: &Vec<Point>) -> i32 {
    let mut min_distance = i32::MAX;

    for point in points {
        if point.distance < min_distance {
            min_distance = point.distance
        }
    }

    min_distance
}

fn get_min_distance_wires(points: &Vec<Point>) -> i32 {
    let mut min_distance = i32::MAX;

    for point in points {
        if point.distance_line < min_distance {
            min_distance = point.distance_line
        }
    }

    min_distance
}
